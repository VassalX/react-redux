import React from 'react';
import MessageList from './Message/MessageList.js';
import { connect } from 'react-redux';
import { addMessage } from './Message/MessageActions';

class Chat extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            messages: this.props.messages
        }

        this.addMessage = this.addMessage.bind(this);
        this.getUsersNumber = this.getUsersNumber.bind(this);
    }

    getUsersNumber() {
        const users = this.props.messages.map(mess => mess.user);
        const uniqueUsers = users.filter((value, index, self) => {
            return self.indexOf(value) === index;
        });
        return uniqueUsers.length;
    }

    addMessage(e) {
        e.preventDefault();
        if (this._inputElement.value !== "") {
            const d = new Date();
            let newMessage = {
                message: this._inputElement.value,
                created_at: d.toLocaleString(),
                user: "Vasya",
                id: Date.now()
            }

            this.props.addMessage(newMessage);

            this._inputElement.value = "";
        }
    }

    render() {
        return (<div id="chat">
            <div id="header">
                <div className="chat-name"> {this.props.chatName}
                </div>
                <div className="user-num-wrap">
                    Users:
        <span id="user-num"> {this.getUsersNumber()}
                    </span>
                </div>
                <div className="message-num-wrap">
                    Messages:
        <span id="message-num"> {this.props.messages.length || 0}
                    </span>
                </div>
            </div>
            <MessageList user="Vasya" />
            <form className="message-form" onSubmit={this.addMessage}>
                < input type="text" name="message" ref={(a) => this._inputElement = a} id="message-input" placeholder="Message" />
                <button type="submit" id="message-submit"> Submit
        </button>
            </form>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages
    }
}

const mapDispatchToProps = {
    addMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);