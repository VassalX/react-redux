import { all } from 'redux-saga/effects';
import messageSagas from '../Message/sagas';

export default function* rootSaga() {
    yield all([
        messageSagas()
    ]);
};