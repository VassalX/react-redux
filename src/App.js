import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import Chat from './Chat.js';
import EditPage from './EditPage/EditPage';

class App extends React.Component {

  render() {
    return (
      <Switch>
        <Route exact path="/" component={Chat} />
        <Route path="/edit/:id" component={EditPage} />
      </Switch>
      //user="Vasya"
    )
  }
}

export default App;
