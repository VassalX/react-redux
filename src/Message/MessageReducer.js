import { FETCH_MESSAGES_SUCCESS } from './MessageActionTypes';

const initialState = [];

export default function(state = initialState, action) {
    switch (action.type) {
        case FETCH_MESSAGES_SUCCESS:
            {
                return [...action.payload.messages]
            }
        default:
            return state;
    }
}