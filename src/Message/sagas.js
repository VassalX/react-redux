import axios from 'axios';
import api from '../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { ADD_MESSAGE, DELETE_MESSAGE, UPDATE_MESSAGE, FETCH_MESSAGES } from './MessageActionTypes';

export function* fetchMessages() {
    try {
        const messages = yield call(axios.get, `${api.url}/message`);
        yield put({
            type: 'FETCH_MESSAGES_SUCCESS',
            payload: {
                messages: messages.data
            }
        });
    } catch (error) {
        console.log('fetchMessages error:', error.message);
    }
}

function* watchFetchMessages() {
    yield takeEvery(FETCH_MESSAGES, fetchMessages);
}

export function* addMessage(action) {
    const newMessage = {...action.payload.data, id: action.payload.id };

    try {
        yield call(axios.post, `${api.url}/message`, newMessage);
        yield put({ type: FETCH_MESSAGES });
    } catch (error) {
        console.log('createMessage error', error.message);
    }
}

function* watchAddMessage() {
    yield takeEvery(ADD_MESSAGE, addMessage);
}

export function* updateMessage(action) {
    const id = action.payload.id;
    const updateMessage = {...action.payload.data };

    try {
        yield call(axios.put, `${api.url}/message/${id}`, updateMessage);
        yield put({ type: FETCH_MESSAGES });
    } catch (error) {
        console.log('updateMessage error', error.message);
    }
};

function* watchUpdateMessage() {
    yield takeEvery(UPDATE_MESSAGE, updateMessage);
}

export function* deleteMessage(action) {
    try {
        yield call(axios.delete, `${api.url}/message/${action.payload.id}`);
        yield put({ type: FETCH_MESSAGES });
    } catch (error) {
        console.log('deleteMessage error:', error.message);
    }
}

function* watchDeleteMessage() {
    yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

export default function* usersSagas() {
    yield all([
        watchAddMessage(),
        watchDeleteMessage(),
        watchFetchMessages(),
        watchUpdateMessage()
    ])
}