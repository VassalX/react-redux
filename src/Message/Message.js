import React from 'react';

const Message = ({ messageData, thisUser, onEdit, onDelete, key }) => {
  const { id, avatar, created_at, user, message } = messageData;
  let className;
  let control;
  let isSelf = user === thisUser;
  console.log(user);
  console.log(thisUser);
  console.log(isSelf);
  if (isSelf) {
    className = "your";
    control = (<div className="message-control">
      <button className="edit control" onClick={() => { onEdit(id) }}>Edit</button>
      <button className="delete control" onClick={() => { onDelete(id) }}>Delete</button>
    </div>);
  } else {
    className = "their";
    control = (<div className="message-control">
      <button className="likes-wrap control">
        Likes
            <span className="likes-num">{}</span>
      </button>
    </div>)
  }
  return (
    <div key={key} className={"message " + className}>
      {
        !isSelf &&
        <img className="avatar" src={avatar} alt="avatar"></img>
      }
      <div className="text">
        <div><b>{user}</b> {created_at}</div>
        {message}
      </div>
      <div className="message-control">
        {control}
      </div>
    </div>
  );
}

export default Message;