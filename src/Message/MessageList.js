import React from 'react';
import Message from './Message.js';
import { connect } from 'react-redux';
import * as actions from './MessageActions';

class MessageList extends React.Component {

        constructor(props) {
            super(props);
            this.onDelete = this.onDelete.bind(this);
            this.onEdit = this.onEdit.bind(this);
        }

        componentDidMount() {
            this.props.fetchMessages();
        }

        onEdit(id) {
            this.props.history.push(`/message/${id}`);
        }

        onDelete(id) {
            this.props.deleteMessage(id);
        }

        render() {
            /*
            if (loading) {
                return <img id="loading" src="../data/circle.gif" />;
            }
            */
            console.log(this.props.messages);
            console.log(this.props.user);
            return ( <div id = "message-list" > {
                    this.props.messages
                    .map((message, index) => {
                        return Message({
                            messageData: message,
                            thisUser: this.props.user,
                            onDelete: this.onDelete,
                            onEdit: this.onEdit,
                            key: index
                        });
                    })
                } </div>)
            }
        }

        const mapStateToProps = (state) => {
            return {
                messages: state.messages
            }
        }

        const mapDispatchToProps = {
            ...actions
        }

        export default connect(mapStateToProps, mapDispatchToProps)(MessageList);