import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGES } from "./MessageActionTypes";

export const addMessage = data => ({
    type: ADD_MESSAGE,
    payload: {
        id: Date.now(),
        data
    }
});

export const updateMessage = (id, data) => ({
    type: UPDATE_MESSAGE,
    payload: {
        id,
        data
    }
})

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
})

export const fetchMessages = data => ({
    type: FETCH_MESSAGES
})