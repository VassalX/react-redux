import { FETCH_MESSAGE_SUCCESS } from "./EditPageActionTypes";

const initialState = {
    messageData: {
        id: '',
        user: '',
        created_at: '',
        message: ''
    }
}

export default function(state = initialState, action) {
    switch (action.type) {
        case FETCH_MESSAGE_SUCCESS:
            {
                const { messageData } = action.payload;
                return {
                    ...state,
                    messageData
                }
            }
        default:
            return state;
    }
}