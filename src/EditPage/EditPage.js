import React from 'react';
import { connect } from 'react-redux';
import {updateMessage} from '../Message/MessageActions';
import * as actions from './EditPageActions';
import defaultMessageConfig from '../shared/config/defaultMessageConfig';

class EditPage extends React.Component{
    constructor(props){
        super(props);
        this.onCancel = this.onCancel.bind(this);
        this.onSave = this.onSave.bind(this);
    }

    componentDidMount(){
        if(this.props.match.params.id){
            this.props.fetchMessage(this.props.match.params.id);
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.messageData.id !== prevState.id && nextProps.match.params.id) {
            return {
                ...nextProps.messageData
            };
        } else {
            return null;
        }
    }

    getDefaultMessageData() {
        return {
            ...defaultMessageConfig
        };
    }

    onCancel(e){
        e.preventDefault();
        this.setState(this.getDefaultMessageData());
        this.props.history.push('/');
    }

    onSave(e){
        e.preventDefault();
        this.props.updateMessage(this.state.id, this.state);
        this.props.updateMessage(this.props.messageId, {message:this._inputElement.value});
        this.setState(this.getDefaultMessageData());
        this.props.history.push('/');
    }

    getEditPageContent(){
        return (
            <div id="edit">
                <form className="message-form" onSubmit={this.onSave}>
                    <input type="text" name="message" ref={(a) => this._inputElement = a} id="message-input" placeholder="Message" />
                    <button type="submit" id="message-submit">Edit</button>
                    <button id="cancel" onClick={this.onCancel}>X</button>
                </form>
            </div>
        )
    };

    render() {
        return this.getEditPageContent();
    }
}

const mapStateToProps = (state) => {
    return {
        messageData: state.editPage.messageData
    }
}

const mapDispatchToProps = {
    ...actions,
    updateMessage
}

export default connect(mapStateToProps,mapDispatchToProps)(EditPage);