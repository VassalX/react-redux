import { FETCH_MESSAGE } from "./EditPageActionTypes";

export const fetchMessage = id => ({
    type: FETCH_MESSAGE,
    payload: {
        id
    }
});