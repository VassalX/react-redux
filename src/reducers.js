import { combineReducers } from 'redux';
import messages from './Message/MessageReducer';
import editPage from './EditPage/EditPageReducer';

const rootReducer = combineReducers({
    editPage,
    messages
});

export default rootReducer;