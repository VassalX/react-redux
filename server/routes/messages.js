var express = require('express');
var router = express.Router();

const {
    getAllMessages,
    getMessageById,
    addMessage,
    updateMessage,
    deleteMessage
} = require("../services/message.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

router.get('/', function(req, res, next) {
    const result = getAllMessages();

    if (result) {
        res.send(result);
    } else {
        res.status(400).send(`Could not get list of messages!`);
    }
});

router.get('/:id', function(req, res, next) {
    const id = req.params.id;
    const result = getMessageById(id);

    if (result) {
        res.send(result);
    } else {
        res.status(400).send(`Could not get message with id: ${id}!`);
    }
});

router.post('/', function(req, res, next) {
    const result = addMessage(req.body);

    if (result) {
        res.send(`New message have been added!`);
    } else {
        res.status(400).send(`Could not add new message!`);
    }
});

router.put('/:id', function(req, res, next) {
    const id = req.params.id;
    const result = updateMessage(req.body, id);

    if (result) {
        res.send(`Message with id: ${id} have been updated!`);
    } else {
        res.status(400).send(`Could not update message with id: ${id}!`);
    }
});

router.delete('/:id', function(req, res, next) {
    const id = req.params.id;
    const result = deleteMessage(id);

    if (result) {
        res.send(`Message with id: ${id} have been deleted!`);
    } else {
        res.status(400).send(`Could not delete message with id: ${id}!`);
    }
});

module.exports = router;