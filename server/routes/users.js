var express = require('express');
var router = express.Router();

const {
    getAllUsers,
    getUserById,
    addUser,
    updateUser,
    deleteUser
} = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

router.get('/', function(req, res, next) {
    const result = getAllUsers();

    if (result) {
        res.send(result);
    } else {
        res.status(400).send(`Could not get list of users!`);
    }
});

router.get('/:id', function(req, res, next) {
    const id = req.params.id;
    const result = getUserById(id);

    if (result) {
        res.send(result);
    } else {
        res.status(400).send(`Could not get user with id: ${id}!`);
    }
});

router.post('/', function(req, res, next) {
    const result = addUser(req.body);

    if (result) {
        res.send(`New user have been added!`);
    } else {
        res.status(400).send(`Could not add new user!`);
    }
});

router.put('/:id', function(req, res, next) {
    const id = req.params.id;
    const result = updateUser(req.body, id);

    if (result) {
        res.send(`User with id: ${id} have been updated!`);
    } else {
        res.status(400).send(`Could not update user with id: ${id}!`);
    }
});

router.delete('/:id', function(req, res, next) {
    const id = req.params.id;
    const result = deleteUser(id);

    if (result) {
        res.send(`User with id: ${id} have been deleted!`);
    } else {
        res.status(400).send(`Could not delete user with id: ${id}!`);
    }
});

module.exports = router;