const MessagesRepo = require("../repositories/message.repository");

const isMessage = (message) => {
    if (
        message &&
        message.id &&
        message.user &&
        message.created_at &&
        message.message
    ) {
        return true;
    } else {
        return false;
    }
}

const getAllMessages = () => {
    console.log("getAllMessages");
    return MessagesRepo.getAllMessages();
};

const getMessageById = (id) => {
    console.log("getMessageById");
    if (id) {
        return MessagesRepo.getMessageById(id);
    } else {
        return null;
    }
};

const addMessage = (message) => {
    console.log("addMessage");
    if (isMessage(message)) {
        return MessagesRepo.addMessage(message);
    } else {
        return null;
    }
}

const updateMessage = (message, id) => {
    console.log("updateMessage");
    if (isMessage(message) && id && message.id == id) {
        return MessagesRepo.updateMessage(message, id);
    } else {
        return null;
    }
}

const deleteMessage = (id) => {
    console.log("deleteMessage");
    if (id) {
        return MessagesRepo.deleteMessage(id);
    } else {
        return null;
    }
}

module.exports = {
    getAllMessages,
    getMessageById,
    addMessage,
    updateMessage,
    deleteMessage
};