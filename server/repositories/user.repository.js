const fs = require('fs');
const user_filename = 'assets/data/userlist.json'

const readUsersList = () => {
    try {
        const rawdata = fs.readFileSync(user_filename);
        const users = JSON.parse(rawdata);
        return users;
    } catch (err) {
        throw err;
    }
}

const writeUsersList = (data) => {
    try {
        fs.writeFileSync(user_filename, JSON.stringify(data));
    } catch (err) {
        throw err;
    }
}

const isUserWithId = (id, user) => {
    return user.id == id;
}

const getAllUsers = () => {
    try {
        const data = readUsersList();
        return data;
    } catch (err) {
        console.log(err);
        return null;
    }
};



const getUserById = (id) => {
    try {
        checkUserId(id);
        const data = readUsersList();
        checkUserData(data);
        const user = data.find((user) => isUserWithId(id, user));
        return user;
    } catch (err) {
        console.log(err);
        return null;
    }
};

const addUser = (user) => {
    try {
        checkUser(user);
        let data = readUsersList();
        checkUserData(data);
        checkNotExistsUser(data, user);
        data.push(user);
        writeUsersList(data);
        return true;
    } catch (err) {
        console.log(err);
        return false;
    }
}

const updateUser = (user, id) => {
    try {
        checkUser(user);
        checkUserId(id);
        let data = readUsersList();
        checkUserData(data);
        const index = data.findIndex((user) => isUserWithId(id, user));
        checkUserIndex(index);
        data[index] = user;
        writeUsersList(data);
        return true;
    } catch (err) {
        console.log(err);
        return false;
    }
}

const deleteUser = (id) => {
    try {
        checkUserId(id);
        let userList = readUsersList();
        checkUserData(userList);
        const index = userList.findIndex((user) => isUserWithId(id, user));
        checkUserIndex(index);
        userList.splice(index, 1);
        writeUsersList(userList);
        return true;
    } catch (err) {
        console.log(err);
        return false;
    }
}

const checkUserIndex = (index) => {
    if (index < 0) {
        throw new Error(`User not found!`);
    }
}

const checkUserId = (id) => {
    if (!id) {
        throw new Error(`Incorrect id!`);
    }
}

const checkUserData = (data) => {
    if (!data) {
        throw new Error(`No user data found!`);
    }
}

const checkUser = (user) => {
    if (!user) {
        throw new Error(`Incorrect user!`);
    }
}

const checkNotExistsUser = (data, user) => {
    const id = user.id;
    const exUser = data.find((user) => isUserWithId(id, user));
    if (exUser) {
        throw (new Error(`User with id: ${id} already exists`));
    }
}

module.exports = {
    getAllUsers,
    getUserById,
    addUser,
    updateUser,
    deleteUser
};