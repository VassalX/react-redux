const fs = require('fs');
const message_filename = 'assets/data/messagelist.json';

const readMessagesList = () => {
    try {
        const rawdata = fs.readFileSync(message_filename);
        const messages = JSON.parse(rawdata);
        return messages;
    } catch (err) {
        throw err;
    }
}

const writeMessagesList = (data) => {
    try {
        fs.writeFileSync(message_filename, JSON.stringify(data));
    } catch (err) {
        throw err;
    }
}

const isMessageWithId = (id, message) => {
    return message.id == id;
}

const getAllMessages = () => {
    try {
        const data = readMessagesList();
        return data;
    } catch (err) {
        console.log(err);
        return null;
    }
};



const getMessageById = (id) => {
    try {
        checkMessageId(id);
        const data = readMessagesList();
        checkMessageData(data);
        const message = data.find((message) => isMessageWithId(id, message));
        return message;
    } catch (err) {
        console.log(err);
        return null;
    }
};

const addMessage = (message) => {
    try {
        checkMessage(message);
        let data = readMessagesList();
        checkMessageData(data);
        checkNotExistsMessage(data, message);
        data.push(message);
        writeMessagesList(data);
        return true;
    } catch (err) {
        console.log(err);
        return false;
    }
}

const updateMessage = (message, id) => {
    try {
        checkMessage(message);
        checkMessageId(id);
        let data = readMessagesList();
        checkMessageData(data);
        const index = data.findIndex((message) => isMessageWithId(id, message));
        checkMessageIndex(index);
        data[index] = message;
        writeMessagesList(data);
        return true;
    } catch (err) {
        console.log(err);
        return false;
    }
}

const deleteMessage = (id) => {
    try {
        checkMessageId(id);
        let messageList = readMessagesList();
        checkMessageData(messageList);
        const index = messageList.findIndex((message) => isMessageWithId(id, message));
        checkMessageIndex(index);
        messageList.splice(index, 1);
        writeMessagesList(messageList);
        return true;
    } catch (err) {
        console.log(err);
        return false;
    }
}

const checkMessageIndex = (index) => {
    if (index < 0) {
        throw new Error(`Message not found!`);
    }
}

const checkMessageId = (id) => {
    if (!id) {
        throw new Error(`Incorrect id!`);
    }
}

const checkMessageData = (data) => {
    if (!data) {
        throw new Error(`No message data found!`);
    }
}

const checkMessage = (message) => {
    if (!message) {
        throw new Error(`Incorrect message!`);
    }
}

const checkNotExistsMessage = (data, message) => {
    const id = message.id;
    const exMessage = data.find((message) => isMessageWithId(id, message));
    if (exMessage) {
        throw (new Error(`Message with id: ${id} already exists`));
    }
}

module.exports = {
    getAllMessages,
    getMessageById,
    addMessage,
    updateMessage,
    deleteMessage
};